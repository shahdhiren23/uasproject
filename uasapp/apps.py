from django.apps import AppConfig


class UasappConfig(AppConfig):
    name = 'uasapp'
